from django.urls import path,include
from rest_framework import routers

from .rest_views import *

from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('', UserViewSet,)
# router.register('product',ProductViewset)
# router.register('orders',OrdersViewset,basename="Orders")
# router.register('order_items',Order_item_Viewset)
# router.register('register', UserViewSet)

urlpatterns=[
    path('register/',include(router.urls)),
    path('login/',obtain_auth_token),
    path('orders/',OrderListAPi.as_view()),
    path('orders/<int:pk>/', OrderListAPi.as_view()),
    path('products/', ProductApi.as_view()),
    path('products/<int:pk>/', ProductApi.as_view()),
    path('orderitems/', OrderItemsAPi.as_view()),
    path('orderitems/<int:pk>/', OrderItemsAPi.as_view()),

]
